    #!/bin/bash

    : '
    Simple test parsing script
    '

    NULL=""
    PROMPT=">"
    SPACING="============================="

    executable=$1

    test_opts=('test' 'tests' 'teste' 'testes')
    test_dir=NULL

    output_opts=('output' 'outputs' 'out' 'outs')
    output_dir=NULL

    my_output_dir=NULL

    #==============================================================================
    #Executable processing and setup

    if [[ $# -eq 0 || "$executable" == NULL ]]; then
        echo "No executable has been specified, please input: "
        read -p $PROMPT executable
    fi

    [[ -x "$executable" ]] && found=true || found=false 

    while ! $found; do
        echo "The specified file did not exist, please try again: "
        read -p $PROMPT executable

        [[ -x "$executable" ]] && found=true
    done

    #==============================================================================
    #Directory processing and setup

    #Searches for the test directory by name (within the denominations alloted), and, if found sets the 'test_dir' variable accordingly.
    for dir in *; do
        for name in ${test_opts[@]}; do
            [[ "$dir" == "$name" && -d "$dir" ]] && test_dir=$dir
        done
    done

    if [[ $test_dir == NULL ]]; then
        echo "No test directory has been found, please supply one with an alloted name."
        exit 1
    fi

    for dir in $test_dir/*; do
        for out_name in ${output_opts[@]}; do
            [[ "$dir" == "$test_dir/$out_name" && -d "$dir" ]] && output_dir=$out_name

            [[ "$dir" == "$test_dir/my_$out_name" && -d "$dir" ]] && my_output_dir="my_$out_name"
        done
    done

    if [[ $output_dir == NULL ]]; then
        echo "No ouput directory has been found, please supply one with an alloted name."
        exit 1
    fi

    if [[ $my_output_dir == NULL ]]; then
        mkdir "$test_dir/my_$output_dir"

    #If the user's output file does not match the defined output file, then the user's filename is changed 
    elif [[ "$my_output_dir" != "my_$output_dir" ]]; then
        mv "$test_dir/$my_output_dir" "$test_dir/my_$output_dir"
    fi

#==============================================================================
#Set test, and output names.

i=1

for file in $test_dir/*; do

    if [[ -f $file ]]; then
        [[ "$file" != "$test_dir/$i" ]] && mv $file $test_dir/$i

        ./$executable $file > $test_dir/$my_output_dir/$i".myout"

        res=$(diff -N $test_dir/$my_output_dir/$i".myout" $test_dir/$output_dir/$i".out")
 
        if [[ $? -eq 0 ]]; then
            printf "\n$SPACING \n\tPassed Test %d \n$SPACING \n" $i

        else
            printf "\n$SPACING \n\tFailed Test %d \n$SPACING \n" $i 
        fi

        ((i++))
    fi
done